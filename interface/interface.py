import tkinter as tk
from tkinter import messagebox
import glob, os, string

instance = False

class TkinterInterface(tk.Tk):

    # contient les fichiers / dossiers sélectionnés [ leur background sera en bleu et leur texte en blanc ]
    selectedFile = []
    refreshMethod = '' # va contenir la méthode enfant pour rafraîchir le listing des fichiers/dossiers

    def __init__(self, *args, **kwargs):
        global instance

        # Vu que la class TkinterInterface est une Class qui hérite de tk et qui elle-même se fait hériter par les classes filles manager et menu nous devons
        # empecher de repasser par le contructor car celui-ci crée des multitudes de fenêtres pour chaque nouveaux appellent du constructor parent
        if(instance == False):
            super().__init__(*args, **kwargs)
            instance = True

    def setTitle(self,title):
        self.__title = title

    def _getSelectedFile(self):
        return TkinterInterface.selectedFile

    def _addSelectedFile(self,file_name):
        TkinterInterface.selectedFile.append(file_name)

    def _removeSelectedFile(self,file_name):
        TkinterInterface.selectedFile.remove(file_name)

    def __createInterface(self):
        from component.menu.menu import Menu
        from component.manager.manager import Manager
        # création des menu lib.interface.interface.TkinterInterface()
        menuInterface = Menu(self)
        menuInterface.createMenu()
        # création du manager
        managerInterface = Manager(self)
        managerInterface.createScreen()

    def _deleteFiles(self) :

       files = ''

       for file in self._getSelectedFile():
           files = '{}\n{}'.format(files,file)

       result = messagebox.askquestion("Delete", "Vous êtes sur de vouloir supprimer ces fichiers/dossiers \n{}".format(files), icon='warning')

       error_msg_delete = ''

       file_delete_success = ''

       if result == 'yes':
           for file in self._getSelectedFile():
             if os.path.exists(file):
                if os.path.isfile(file):
                    try:
                        os.remove(file)
                        file_delete_success = '{}\nFichier supprimé : {}\n'.format(file_delete_success,file)
                    except OSError as e:
                        print(e)
                        error_msg_delete = '{}\nCe fichier {} n\'a pas pu être supprimé pour cause : {}\n'.format(error_msg_delete,file,e)
                else:
                    try:
                       os.rmdir(file)
                       file_delete_success = '{}\nDossier supprimé : {}\n'.format(file_delete_success,file)
                    except OSError as e:
                       print(e)
                       error_msg_delete = '{}\nCe dossier {} n\'a pas pu être supprimé pour cause : {}\n'.format(error_msg_delete,file,e)

           if not error_msg_delete: # si tous les fichiers/dossiers ont été supprimés
                return messagebox.showwarning("Bravo", "Cliquer sur le bouton Rafraîchir le listing \n Tous les fichiers/dossiers ont été supprimés")
           print(error_msg_delete,file_delete_success)
           return messagebox.showwarning("Warning", "Cliquer sur le bouton Rafraîchir le listing \n {}{}".format(file_delete_success,error_msg_delete))

        
    def create(self):
        self.resizable(0, 0)
        self.geometry("1000x500")
        self.resizable(width=True, height=True)
        # configure the grid
        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=5)

        self.__interfaceTk = tk.Frame(self)

        self.__createInterface()
        self.mainloop()
        
        
