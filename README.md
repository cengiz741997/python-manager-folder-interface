**Manager de dossier**

---

## Actions possibles

1. ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) Supprimer un ou plusieurs fichiers/dossiers en double-cliquant ils seront sélectionnés.
2. ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) Rafraîchir les drivers disponibles ( ex C, D ...) .
3. ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) Rafraîchir le listing des fichiers/dossiers sur le current PATH.
4. ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) Role-back sur le ( revenir en arrière ) PATH
5. ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) Ouvrir des fichiers en double-cliquant dessus.

---

## Lancer l'application

2 manières d'ouvrir l'application.

1. Se rentre sur la racine du projet et lancer <u>py main.py</u>
<br/>ou
2. Allez dans le dossier dist et cliquer sur <u>main.exe</u>

---

## Démo

Interface

![Screenshot](img/interface.PNG)

---

1. ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) Supprimer un ou plusieurs fichiers/dossiers en double-cliquant ils seront sélectionnés.

![Screenshot](img/delete.PNG)

---

2. ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) Rafraîchir les drivers disponibles ( ex C, D ...) .

![Screenshot](img/driver.PNG)

---

3. ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) Rafraîchir le listing des fichiers/dossiers sur le current PATH.

![Screenshot](img/pathPerso.PNG)

---

4. ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) Role-back sur le ( revenir en arrière ) PATH

![Screenshot](img/travel.PNG)

---

5. ![#c5f015](https://via.placeholder.com/15/c5f015/000000?text=+) Ouvrir des fichiers en double-cliquant dessus.

![Screenshot](img/openFile.PNG)