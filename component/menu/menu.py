
import tkinter as tk
from tkinter import ttk
from interface.interface import TkinterInterface

# est le menu présent à gauche sur le programme
class Menu(TkinterInterface):
    def __init__(self,root,*args,**kwargs):
        super().__init__(*args, **kwargs)
        self.__root = root
        self.__menus = [
            {
                'title' : 'Renommer',
                'padx' : 5,
                'pady' : 5,
                'method' : False,
                'state' : False
             },
             {
                'title' : 'Supprimer',
                'padx' : 5,
                'pady' : 5,
                'method' : self._deleteFiles,
                'state' : True
             }
        ]
        
    def createMenu(self):
        frameMenu = tk.Frame(width=100)
        frameMenu.grid(column=0,row=0,sticky=tk.NW,padx=5,pady=5)
        for i in range(len(self.__menus)):
            current_menu = self.__menus[i]
            title = current_menu['title']
            x_position = current_menu['padx']
            y_position = current_menu['pady']
            btn = ''

            btn = tk.Button(frameMenu,text=title,height = 3,
                            width = 20, state="disabled")

            if current_menu['method'] :
                btn.configure(command=current_menu['method'])

            if current_menu['state'] :
                btn.configure(state="normal")

            btn.grid(column=0,row=i,sticky=tk.W,padx=x_position,pady=y_position)
       
    
