import tkinter as tk
import glob, os, string
from tkinter import messagebox
from tkinter import *
from PIL import ImageTk, Image

from component.scrollbar.scrollbar import ScrollableFrame
from interface.interface import TkinterInterface

# représente la gestion et l'affichage des fichiers et dossiers 
class Manager(TkinterInterface):
    def __init__(self,root,*args,**kwargs):
        super().__init__(*args, **kwargs)

        self.__root = root
        self.__drives = []
        self.__choiceDriver = tk.StringVar(self.__root)
        # input drive
        self.__driverInput = ''
        # current drive choice by user
        self.__currentDrive = tk.StringVar(self.__root , '')
        self.__findDrives()

        # contient tous les Path parcouru
        self.__traveledPath = []

    def __findDrives(self):
        self.__traveledPath = []
        self.__drives = ['%s:\\' % d for d in string.ascii_uppercase if os.path.exists('%s:' % d)]
        self.__choiceDriver.set(self.__drives[0])
        if self.__driverInput:
            self.__refreshListLocatePath()
            self.__traveledPath.append(self.__drives[0])
            print(self.__traveledPath)

    def __setCurrentMenu(self,*args):
        self.__currentDrive.set(self.__choiceDriver.get())
        self.__refreshListLocatePath()

    def __openFile(self,element):
        name_of_file = element.widget.cget('text')
        root_to_path = "{}{}".format(self.__driverInput.get(),name_of_file)
        if not os.path.exists(root_to_path):
            return messagebox.showerror("Error", "Le fichier n'existe pas")

        try:
           k = os.startfile(root_to_path)
        except OSError as e:
           return messagebox.showerror("Error", "{}".format(e))


    def __selecteFile(self,element):
        name_of_file = "{}{}".format(self.__driverInput.get(),element.cget('text'))

        if name_of_file in self._getSelectedFile(): # si il est présent dans le array des fichiers sélectionnés on le remove ( il devient unselect )
            self._removeSelectedFile(name_of_file)
            element.config(background="#FFFFFF")
        else:
            self._addSelectedFile("{}".format(name_of_file))
            element.config(background="#2BC2FD")

    def __roleBackPath(self):

        len_traveled_path = len(self.__traveledPath)
        if(len_traveled_path <= 1) :
            return messagebox.showwarning("Warning", "Vous ne pouvez revenir en arrière vous avez atteint la limite")

        del self.__traveledPath[-1]
        self.__currentDrive.set(self.__traveledPath[len(self.__traveledPath)-1])
        self.__refreshListLocatePath()

    def createScreen(self):

        self._refresh_method = TkinterInterface.refreshMethod

        framePath = tk.Frame(self.__root)
        framePath.grid(column=1,row=0,sticky=tk.NW,padx=2,pady=5)

        # menu déroulant driver
        opt = tk.OptionMenu(framePath,self.__choiceDriver,*self.__drives,command=self.__setCurrentMenu)
        opt.config(width=2)
        opt.grid(column=1,row=1,sticky=tk.NW,padx=20,pady=5)

        # label PATH
        label = tk.Label(framePath,text="CURRENT PATH", fg='green')
        label.grid(column=2,row=1,sticky=tk.NW,padx=15,pady=5)

        # input PATH locate
        self.__driverInput = tk.Entry(framePath,textvariable=self.__currentDrive,width=70)
        self.__driverInput.grid(column=3,row=1,sticky=tk.N,padx=2,pady=5)
        self.__choiceDriver.trace("w",self.__setCurrentMenu)
        # btn qui permet de rechercher les fichier via un path dans le Entry de self.__driverInput
        btn_search_path = tk.Button(framePath,text="Charger",command=self.__refreshListLocatePath)
        btn_search_path.grid(column=4,row=1,sticky=tk.NE,padx=5,pady=5)

        # refresh btn for find driver
        btn_refresh = tk.Button(framePath,text="Refresh driver",command=self.__findDrives)
        btn_refresh.grid(column=1,row=2,sticky=tk.W,padx=20,pady=5)

        self.__currentDrive.set(self.__drives[0])
        self.__findDrives()

    def __updateElement(self,element,array):
        for k, v in array.items():
            element[k] = v
        return element

    def addTargetPath(self,element):

       # vu qu'on dispose déjà d'un event de simple clique sur les dossiers pour les sélectionnées et vue qu'a dispées d'un autre event double click
       # il faut enlever de l'array
        name_folder = element.widget.cget('text')

        self.__selecteFile(element.widget)

        last_string_name = len(name_folder) - 1
        last_string_name = name_folder[last_string_name]

        if last_string_name != '\\':
            name_folder = "{}{}".format(name_folder,'\\')
        new_path = "{}{}".format(self.__currentDrive.get(),name_folder)
        self.__currentDrive.set(new_path)
        self.__traveledPath.append(new_path)
        return self.__refreshListLocatePath()


    def __refreshListLocatePath(self):
        if not self.__drives:
            return messagebox.showerror("ERREUR", "Aucun driver trouvé sur votre pc, appuyer sur le btn 'Refresh driver'")

        path = self.__driverInput.get()
        if not os.path.exists(path):
            return messagebox.showerror("ERREUR", "Le chemin spécifier n'existe pas")

        i = 1

        btn_refresh_listing = tk.Button(self.__root,text="Rafraîchir le listing",command=self.__refreshListLocatePath)
        btn_refresh_listing.grid(column=1,row=1,padx=40,pady=5,sticky=tk.NW)
        # le bouton rôle back pour revenir en arrière concernant le PATH
        btn_role_back = tk.Button(self.__root,text="Revenir en arrière",command=self.__roleBackPath)
        btn_role_back.grid(column=1,row=1,padx=5,pady=5,sticky=tk.N)

        # le conteneur qui affiche les dossiers et fichiers
        frameScrollbar = ScrollableFrame(self.__root)
        #frameList = tk.Frame(frameScrollbar,bg="#FFFFFF",width=100,height=40)
        frameScrollbar.grid(column=1,row=2,sticky=tk.NW,padx=30,pady=5)

        try:
            for file in os.scandir(path):
                file_name = file.name
                name = ''
                if os.path.isdir(file) :
                   frameFolder = tk.Frame(frameScrollbar.scrollable_frame,bg="#FFFFFF",width=150,height=40)
                   frameFolder.grid(column=2,row=i,sticky=tk.NW,padx=30,pady=8)
                   type = tk.Label(frameFolder,text='DOSSIER',background="#EFFE39",fg="#242424")
                   type.grid(column=1,row=i,sticky=tk.NW)
                   name = tk.Button(frameFolder,text="{}".format(file.name),anchor='w',width=50,padx=15,
                   bg="#FFFFFF" ,name=str(i),borderwidth=0)

                   if os.path.abspath(file) in self._getSelectedFile():
                        name.configure(background="#2BC2FD")

                   # SI LE FICHIER EST CONTENU DANS LE ARRAY DE FICHIER SELECTIONNES on change sa couleur
                   name.grid(column=3,row=i,sticky=tk.NW)
                   name.bind('<Double-Button-1>', self.addTargetPath)
                   name.configure(command=lambda button=name: self.__selecteFile(button))
                else:
                   frameFile = tk.Frame(frameScrollbar.scrollable_frame,bg="#FFFFFF",width=150,height=40)
                   frameFile.grid(column=2,row=i,sticky=tk.NW,padx=30,pady=8)
                   type = tk.Label(frameFile,text='FICHIER', background="#B3FE57",fg="#242424")
                   type.grid(column=1,row=i,sticky=tk.NW)
                   name = tk.Button(frameFile,anchor='w',text="{}".format(file.name),width=50,padx=15,
                   bg="#FFFFFF" ,name=str(i),borderwidth=0)
                   # SI LE FICHIER EST CONTENU DANS LE ARRAY DE FICHIER SELECTIONNES on change sa couleur
                   name.grid(column=3,row=i,sticky=tk.NW)

                   if os.path.abspath(file) in self._getSelectedFile():
                        name.configure(background="#2BC2FD")

                   name.bind('<Double-Button-1>', self.__openFile)
                   name.configure(command=lambda button=name: self.__selecteFile(button))
                if file_name in self._getSelectedFile():
                    self.__updateElement(name,{'bg' : '#2BC2FD'})
                i+=1

        except OSError as e:
            return messagebox.showerror("ERREUR", "{}".format(e))